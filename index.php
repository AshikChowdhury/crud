<?php
include_once("classes/Crud.php");

$crud = new Crud();

$query = "SELECT * FROM users ORDER BY id DESC";
$result = $crud->getData($query);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Home Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <div style="height:150px"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
                <button class="btn btn-success btn-lg">
                    <a href="add.html" style="color: white; text-decoration: none">Add New Data</a>
                </button><br/><br/>
                <div class="table-responsive">
                    <table class="table table-condensed table-hover table-bordered table-striped">
                        <tr style="font-weight: bolder">
                            <td>Name</td>
                            <td>Age</td>
                            <td>Email</td>
                            <td>Update</td>
                        </tr>
                        <?php
                        foreach ($result as $key => $res){
                            //while($res = mysqli_fetch_array($result)) {
                            echo "<tr>";
                            echo "<td class='info'>".$res['name']."</td>";
                            echo "<td class='info'>".$res['age']."</td>";
                            echo "<td class='info'>".$res['email']."</td>";
                            echo "<td class='info'><a class=\"btn btn-primary btn-xs\" href=\"edit.php?id=$res[id]\">Edit</a>
                  <a class=\"btn btn-danger btn-xs\" href=\"delete.php?id=$res[id]\"
                   onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td></tr>";
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
