<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Add Data</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div style="height:150px"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
                <?php
                include_once ('classes/Crud.php');
                include_once ('classes/Validation.php');

                $crud = new Crud();
                $validation = new Validation();

                if (isset($_POST['Submit'])) {
                    $name = $crud->escape_string($_POST['name']);
                    $age = $crud->escape_string($_POST['age']);
                    $email = $crud->escape_string($_POST['email']);

                    $msg = $validation->check_empty($_POST, array('name', 'age', 'email'));
                    $check_age = $validation->is_age_valid($_POST['age']);
                    $check_email = $validation->is_email_valid($_POST['email']);

                    if ($msg != null) {
                        echo $msg;
                        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
                    }elseif (!$check_age) {
                        echo "<div class='alert alert-warning'>
                      <strong>Please provide proper age.</strong>
                      </div>";
                    }elseif (!$check_email) {
                        echo "<div class='alert alert-warning'>
                      <strong>Please provide proper Email.</strong>
                      </div>";
                    }else{
                        $result = $crud->execute("INSERT INTO users(name, age, email)
                                          VALUES ('$name', '$age', '$email')");

                        echo "<div class='alert alert-success'>
                      <strong>Data Added Successfully!</strong>
                      </div>";
                        echo "<br/><a href='index.php'>View Result</a>";
                    }

                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
