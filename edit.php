<?php

include_once ("classes/Crud.php");

$crud = new Crud();

$id = $crud->escape_string($_GET['id']);

$result = $crud->getData("SELECT * FROM users WHERE id = $id");

foreach ($result as $res){
    $name = $res['name'];
    $age = $res['age'];
    $email = $res['email'];
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Edit Data</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style media="screen">
        body{
            color: #fff;
            background: linear-gradient(rgba(0, 0, 0, 0.45),rgba(0, 0, 0, 0.45)),url(001.jpg);
            /* background-size: cover; */
        }
        form{
            padding-top: 150px;
        }
        form h2{
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
                <h2><a href="index.php">Home</a></h2>
                <form action="editaction.php" method="post" name="form1">
                    <h2 class="text-center">User Registration Form</h2>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $name ?>">
                    </div>
                    <div class="form-group">
                        <label>Age</label>
                        <input type="text" name="age" class="form-control" value="<?php echo $age ?>">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $email ?>">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $_GET['id'] ?>">
                    </div>
                    <button type="submit" name="update" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

