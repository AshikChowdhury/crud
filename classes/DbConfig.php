<?php
class DbConfig
{
    private $_host = 'localhost';
    private $_username = 'root';
    private $_password = '';
    private $_database = 'crud';

    protected $con;

    function __construct()
    {
        if (!isset($this->con)) {
            $this->con = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);

            if (!$this->con) {
                echo "Can not connect the database";
                exit;
            }
        }
        return $this->con;
    }
}

